// Variables y constantes

const nombre = "Sergio";
let apellido = "Díez";
let valorDado = 6;

// Si ya se ha declarado el valor de una variable (let = ...) si se declara posteriormente (con let) se produce un error
// let valorDado = 1;
valorDado = 1;

console.log(nombre, apellido, valorDado);

if (true) {
  // Este valorDado sólo existe dentro de este ámbito (scope)
  let valorDado = 4;
  const nombre ="Toni"

  console.log(valorDado, nombre);
}

console.log(valorDado);
