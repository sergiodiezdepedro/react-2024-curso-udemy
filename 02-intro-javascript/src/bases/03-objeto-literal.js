// Objetos literales

const persona = {
  nombre: "Toni",
  apellido: "Kroos",
  edad: 34,
  direccion: {
    ciudad: "Madrid",
    cp: "28004",
    latitud: 14.3201,
    longitud: 34.92,
  },
};

// console.table( persona );

const persona2 = { ...persona };
persona2.nombre = "Luka";
console.log(persona);
console.log(persona2);
