// Funciones

const saludar = function (nombre) {
  return `Hola, ${nombre}`;
};

const saludar2 = (nombre) => {
  return `Hola, ${nombre}`;
};

// Si la función sólo tiene 1 return, la arrow function se puede declarar así
const saludar3 = (nombre) => `Hola, ${nombre}`;
const saludar4 = () => `Hola, Mundo`;

console.log(saludar("Manolo"));
console.log(saludar2("Lola"));
console.log(saludar3("guapa"));
console.log(saludar4());

const getUser = () => ({
  uid: "ABC321",
  username: "Papi_1984",
});

const user = getUser();

console.log(user);

const getUsuarioActivo = (nombre) => ({
  uid: "WWXY1001",
  username: nombre,
});

const usuarioActivo = getUsuarioActivo("Fernando");
console.log(usuarioActivo);
