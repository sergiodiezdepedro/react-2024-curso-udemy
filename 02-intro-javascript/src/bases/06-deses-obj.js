// Desestructuración de Objetos o Asignación Desestructurante

const persona = {
  nombre: "Tony",
  edad: 45,
  clave: "Ironman",
};

const { edad, clave, nombre } = persona;

// console.log(persona.nombre);
// console.log(persona.edad);
// console.log(persona.clave);

console.log(nombre);
console.log(edad);
console.log(clave);

const userContext = ({ clave, edad, nombre, rango = "Capitán" }) => {
  //   console.log(nombre, edad, rango);

  return {
    nombreClave: clave,
    age: edad,
    latlng: {
      lat: 14.3467,
      lng: 40.5112,
    },
  };
};

// useContext(persona);

// const { nombreClave, age, latlng: {lat, lng}} = userContext(persona);
const { nombreClave, age, latlng } = userContext(persona);
const { lat, lng } = latlng;

console.log(nombreClave, age);
console.log(lat, lng);
