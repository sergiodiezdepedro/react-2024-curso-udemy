// Desestruturación de Arrays

const personajes = ["Capitán América", "Ironman", "Thor"];

// console.log(personajes[0]);
// console.log(personajes[1]);
// console.log(personajes[2]);

const [p1] = personajes;

console.log(p1);

const [, p2] = personajes;
console.log(p2);

const [, , p3] = personajes;
console.log(p3);

// const retornaArray = () => ['ABC', 123];

const retornaArray = () => {
  return ["ABC", 123];
};

const [letras, numeros] = retornaArray();

console.log(numeros, letras);

const userState = (valor) => {
  return [
    valor,
    () => {
      console.log("Hola Mundo");
    },
  ];
};

const [nombre, setNombre] = userState("Kroos");
console.log(nombre);

setNombre();
