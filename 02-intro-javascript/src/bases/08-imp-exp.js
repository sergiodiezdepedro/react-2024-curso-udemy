// import export y funciones comunes de Arrays

import heroes, { owners } from "../data/heroes";

// Lo siguiente funciona, pero no es óptimo
// const getHeroeById = (heroe) => {
//    return heroe.id = 2
// }
// console.log(heroes.find(getHeroeById));

// const getHeroeById = (id) => {
  //   return heroes.find((heroe) => {
    //     if (heroe.id === id) {
      //       return true;
      //     } else {
        //       return false;
        //     }
        //   });
        // };
//Se puede hacer con menos código
        
export const getHeroeById = (id) => {
  return heroes.find((heroe) => heroe.id === id);
};
console.log(getHeroeById(2));

export const getHeroesByOwner = (owner) => {
  return heroes.filter((heroe) => heroe.owner === owner);
};

console.log(getHeroesByOwner("DC"));
// 'find' devuelve el primer resultado que cumpla la condición.
// 'filter' devuelve todos los resultados que cumplan la condición.

console.log(owners);

