// Fetch API

const apiKey = "rMKs6C9SZETl9e6rBNk76IoMpqkqhpS1";

const peticion = fetch(
  `https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`
);

peticion
  .then((resp) => resp.json())
  // Se pueden encadenar Promesas https://es.javascript.info/promise-chaining
  .then(({ data }) => {
    const { url } = data.images.original;
    const img = document.createElement("img");
    img.src = url;
    img.alt = "Un gif de la api de GIPHY";
    document.body.append(img);
  })
  .catch(console.warn);
