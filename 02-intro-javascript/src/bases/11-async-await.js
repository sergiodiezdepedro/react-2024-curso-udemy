// Async - Await

const getImg = async () => {
  try {
    const apiKey = "rMKs6C9SZETl9e6rBNk76IoMpqkqhpS1";

    const resp = await fetch(
      `https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`
    );
    const { data } = await resp.json();
    const { url } = data.images.original;
    const img = document.createElement("img");
    img.src = url;
    img.alt = "Un gif de la api de GIPHY";
    document.body.append(img);
  } catch (error) {
    // Manejo del error
    console.error(error);
  }
};

getImg();
