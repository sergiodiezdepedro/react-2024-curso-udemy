// Operador condicional ternario

const activo = true;

// if (activo) {
//     mensaje = "Activo";
// } else {
//    mensaje = "Inactivo";
// }

// El ternario clásico
// const mensaje = (activo ) ? "Activo" : "Inactivo";

// Sólo si una condición se cumple
const mensaje = activo && "Activo";

console.log(mensaje);
