// import { Fragment } from "react";

// export const FirstApp = () => {
//   return (
//     <Fragment>
//       <h2>FirstApp de pezhammer</h2>
//       <p>Este es un párrafo de texto</p>
//     </Fragment>
//   );
// };

// Otra forma de asignar un nodo padre similar a Fragment
// const newCreator = "pezhammer";
// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de {newCreator}</h2>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

//Devolver valor de una expresión JS
// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de { 10 + 20 }</h2>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

//Devolver una variable numérica
// const newCreator = 99 + 1;
// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de {newCreator}</h2>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

//Devolver un string
// const newCreator = "pezhammer";
// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de {newCreator}</h2>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

//Devolver todo el contenido de un array
// const newCreator = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de {newCreator}</h2>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

// Así se devuelve un objeto entero en React
// const newCreator = {
//     message: "su seguro servidor,",
//     name: "pezhammer"
// };
// export const FirstApp = () => {
//   return (
//     <>
//       <code>FirstApp de { JSON.stringify(newCreator)}</code>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

// Devolver elementos de un objeto sigue siendo válido
// const newCreator = {
//     message: "su seguro servidor,",
//     name: "pezhammer"
// };
// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de {newCreator.message} {newCreator.name}</h2>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

// Devolver una función
// const getCreator = () => {
//     return 'Mighty pezhammer'
// }

// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de {getCreator()}</h2>
//       <p className="featured">Este es un párrafo de texto destacado</p>
//     </>
//   );
// };

// Devolver una función
// const getCreator = (a, b) => {
//     return a + b;
// }
// export const FirstApp = () => {
//   return (
//     <>
//       <h2>FirstApp de {getCreator(50, 25)}</h2>
//       <p>Este es un párrafo de texto</p>
//     </>
//   );
// };

// Warning: FirstApp: Support for defaultProps will be removed from function components in a future major release. Use JavaScript default parameters instead.
import PropTypes from "prop-types";
export const FirstApp = ({ title, subTitle, name }) => {
  return (
    <>
      <h1 data-testid="test-title">{title}</h1>
      <p>{subTitle}</p>
      <p>{subTitle}</p>
      <p className="featured">{name}</p>
    </>
  );
};

FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
};

FirstApp.defaultProps = {
  // Lo siguiente se comenta a efectos de los Tests, para comprobar si hay errores o no
  // title: "No hay título",
  name: "pezhammer",
  subTitle: "No hay subtítulo",
};

// Así queda arreglado sin necesidad de 'defaultProps'
// import PropTypes from 'prop-types';

// export const FirstApp = ({ title = "No hay título", subTitle = "No hay subtítulo", name = "pezhammer" }) => {
//   return (
//     <>
//       <h1>{title}</h1>
//       <p>{subTitle}</p>
//       <p className="featured">{name}</p>
//     </>
//   );
// };

// FirstApp.propTypes = {
//   title: PropTypes.string.isRequired,
//   subTitle: PropTypes.string,
// };
