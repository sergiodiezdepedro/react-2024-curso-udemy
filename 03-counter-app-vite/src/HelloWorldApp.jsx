// export function App() {
//   return <h1>¡¡¡¡Hola Mundo!!!!</h1>;
// }

// export const HelloWorldApp = () => {
//   return (
//     <h1>Hello World App</h1>
//   )
// }

// Como es una función que sólo devuelve esa línea de JSX, se puede abreviar así
export const HelloWorldApp = () => <h1>Hola Mundo. Arrow Function</h1>;
