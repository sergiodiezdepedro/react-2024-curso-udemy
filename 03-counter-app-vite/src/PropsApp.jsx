import PropTypes from "prop-types";

export const PropsApp = ({
  title,
  subTitle = "Sin información suficiente",
  age = 0,
}) => {
  return (
    <>
      <h3>{title}</h3>
      <p>{subTitle}</p>
      <p>{age}</p>
    </>
  );
};

PropsApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  age: PropTypes.number,
};

// PropsApp.defaultProps = {
//   subTitle: "Sin información suficiente",
//   age: 0,
// };

// 'Support for defaultProps will be removed from function components in a future major release. Use JavaScript default parameters instead'. Por esto, no se usa 'defaultProps' y se se definen los valores por defecto de las 'props' no requeridas en la declaración de la función LoremApp, como en vanilla JS.
