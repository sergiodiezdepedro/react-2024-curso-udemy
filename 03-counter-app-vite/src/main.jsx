import React from "react";
import ReactDOM from "react-dom/client";

import { HelloWorldApp } from "./HelloWorldApp";
import { FirstApp } from "./FirstApp";
import { PropsApp } from "./PropsApp";
import "./styles.scss";
import { CounterApp } from "./CounterApp";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* <HelloWorldApp /> */}
    {/* <FirstApp title="Hola, soy un título"/> */}
    {/* <PropsApp title="Toni Kross" subTitle="se retira del fútbol" age={34} /> */}
    {/* <PropsApp title="Victor Roque" /> */}
    <CounterApp value={0}/>
  </React.StrictMode>
);
