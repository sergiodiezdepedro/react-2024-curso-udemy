import { render, screen, fireEvent } from "@testing-library/react";
import { CounterApp } from "../src/CounterApp";

describe("Pruebas en CounterApp />", () => {
  const initialValue = 10;
  test("Debe hacer match con el snapshot", () => {
    const { container } = render(<CounterApp value={initialValue} />);
    expect(container).toMatchSnapshot();
  });

  test("El contador debe mostrar el valor inicial de 100", () => {
    render(<CounterApp value={100} />);
    // expect(screen.getByText("100")).toBeTruthy;
    //   El mismo test limitando la prueba al elemento del DOM que tiene el valor
    expect(screen.getByRole("heading", { level: 2 }).innerHTML).toContain(
      "100"
    );
  });
  test("Debe sumar 1 al pulsar el botón + 1", () => {
    render(<CounterApp value={initialValue} />);
    fireEvent.click(screen.getByText("+ 1"));
    expect(screen.getByText("11")).toBeTruthy();
  });
  test("Debe restar 1 al pulsar el botón - 1", () => {
    render(<CounterApp value={initialValue} />);
    fireEvent.click(screen.getByText("- 1"));
    expect(screen.getByText("9")).toBeTruthy();
  });
  //   test("El botón de reset debe restaurar el valor al de partida", () => {
  //     render(<CounterApp value={initialValue} />);
  //     fireEvent.click(screen.getByText("+ 1"));
  //     fireEvent.click(screen.getByText("+ 1"));
  //     fireEvent.click(screen.getByText("Reset"));
  //     expect(screen.getByText(initialValue)).toBeTruthy();
  //   });

  // Seleccionando el botón de una forma más específica
  test("El botón de reset debe restaurar el valor al de partida", () => {
    render(<CounterApp value={initialValue} />);
    fireEvent.click(screen.getByText("+ 1"));
    fireEvent.click(screen.getByText("+ 1"));
    fireEvent.click(screen.getByRole("button", { name: "btn-reset" }));
    expect(screen.getByText(initialValue)).toBeTruthy();
  });
});
