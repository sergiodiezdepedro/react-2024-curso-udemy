import { render } from "@testing-library/react";
import { FirstApp } from "../src/FirstApp";

describe("Pruebas en <FirstApp />", () => {
  // Hacer tests con 'toMatchSnapshot' sólo cuando el desarrollo esté maduro y no se esperen cambios, porque si no va a fallar si se cambia el código de lo que se testea
  // test("Debe hacer match con el snapshot", () => {
  //     const title= "Hola Mundo";
  //     const {container} = render(<FirstApp title={title}/>);
  //     // console.log(container);
  //     expect(container).toMatchSnapshot();
  // });

  //   test("Debe mostrar el título (title) en un h1", () => {
  //     const title = "Hola Mundo";
  //     const { container, getByText } = render(<FirstApp title={title} />);
  //     expect(getByText(title)).toBeTruthy();
  //     const h1 = container.querySelector("h1");
  //     expect(h1.innerHTML).toContain(title);
  //   });

  test("Debe mostrar el título (title)", () => {
    const title = "Hola Mundo";
    const { container, getByText, getByTestId } = render(
      <FirstApp title={title} />
    );
    expect(getByTestId("test-title").innerHTML).toBe(title);
  });

  test("Debe mostrar el 'subTitle' enviado por props", () => {
    const title = "Hola Mundo";
    const subTitle = "Soy el subtítulo";
    const { getByText } = render(
      <FirstApp title={title} subTitle={subTitle} />
    );
    expect(getByText(subTitle)).toBeTruthy();
  });
});
