import { retornaArreglo } from "../../src/base-pruebas/07-deses-arr";

describe("Pruebas en 07-deses-arr", () => {
  test("Debe retornar un string y un número", () => {
    const [strings, numbers] = retornaArreglo();
    // expect(strings).toBe("ABC");
    // expect(numbers).toBe(123);
    expect(typeof strings).toBe("string");
    expect(typeof numbers).toBe("number");
  });
});
