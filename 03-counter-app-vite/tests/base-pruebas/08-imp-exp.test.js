import { getHeroeById, getHeroesByOwner } from '../../src/base-pruebas/08-imp-exp';

describe("Pruebas en 08-imp-exp", () => {

    test("getHeroeById debe retornar un héroe por ID", () => {
        const id = 1;
        const hero = getHeroeById(id);
        expect(hero).toEqual({ id: 1, name: 'Batman', owner: 'DC' });
    });

    test("getHeroeById debe retornar 'undefined' si no existe el ID", () => {
        const id = 100;
        const hero = getHeroeById(id);
        expect(hero).toBeFalsy();
    });

    test("getHeroesByOwner debe devolder héroes DC", () => {
        const owner = "DC";
        const heroes = getHeroesByOwner(owner);
        expect(heroes.length).toBe(3);
        expect(heroes).toEqual(heroes.filter((heroe) => heroe.owner === owner));
        // console.log(heroes);
    })

    test("getHeroesByOwner debe devolder héroes Marvel", () => {
        const owner = "Marvel";
        const heroes = getHeroesByOwner(owner);
        expect(heroes.length).toBe(2);
        expect(heroes).toEqual(heroes.filter((heroe) => heroe.owner === owner));
        // console.log(heroes);
    })
})
