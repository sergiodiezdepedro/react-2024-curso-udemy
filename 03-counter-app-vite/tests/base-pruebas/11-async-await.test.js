import { getImagen } from "../../src/base-pruebas/11-async-await";

// describe("", () => {
//   test("", () => {});
// });

describe("Pruebas en 11-async-wait", () => {
  test("'getImage debe devolver la url de la imagen", async () => {
    const url = await getImagen();
    console.log(url);
    expect(typeof url).toBe("string");
  });
});
