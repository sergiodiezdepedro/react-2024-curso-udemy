// import { useEffect } from "react";
import { useForm } from "../hooks/useForm";

export const FormWithCustomHook = () => {
  const { onInputChange, onResetForm, username, email, password } =
    useForm({
      username: "",
      email: "",
      password: "",
    });

  // const { username, email, password } = formState;

  return (
    <>
      <h1>Formulario con Custom Hook</h1>
      <hr />
      <input
        className="form-control mb-3"
        type="text"
        placeholder="User Name"
        name="username"
        value={username}
        onChange={onInputChange}
      />
      <input
        className="form-control mb-3"
        type="email"
        placeholder="user@acme.com"
        name="email"
        value={email}
        onChange={onInputChange}
      />
      <input
        className="form-control mb-3"
        type="password"
        placeholder="Contraseña"
        name="password"
        value={password}
        onChange={onInputChange}
      />
      <button onClick={onResetForm} className="btn btn-primary mt-2">
        Borrar
      </button>
    </>
  );
};
