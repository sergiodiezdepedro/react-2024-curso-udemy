import { useState, useEffect } from "react";
import { Message } from "./Message";

export const SimpleForm = () => {
  const [formState, setFormState] = useState({
    username: "pezhammer",
    email: "pezhammer@gmail.com",
  });

  const { username, email } = formState;

  const onInputChange = ({ target }) => {
    const { name, value } = target;
    setFormState({
      ...formState,
      [name]: value,
    });
  };

  useEffect(() => {}, []);
  useEffect(() => {}, [formState]);
  useEffect(() => {}, [email]);

  return (
    <>
      <h1>Simple Form</h1>
      <hr />
      <input
        className="form-control mb-3"
        type="text"
        placeholder="User Name"
        name="username"
        value={username}
        onChange={onInputChange}
      />
      <input
        className="form-control mb-3"
        type="email"
        placeholder="user@acme.com"
        name="email"
        value={email}
        onChange={onInputChange}
      />
      {username === "pezhammer2" && <Message />}
    </>
  );
};
