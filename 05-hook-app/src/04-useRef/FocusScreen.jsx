import { useRef } from "react";

export const FocusScreen = () => {
  const inputRef = useRef();

  const onClick = () => {
    inputRef.current.select();
  };
  return (
    <>
      <h1>Focus Screen</h1>
      <hr />
      <input
        ref={inputRef}
        className="form-control mb-3"
        type="text"
        name=""
        placeholder="Introduzca su nombre"
      />
      <button className="btn btn-dark" onClick={onClick}>
        Set Focus
      </button>
    </>
  );
};
