import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { MainApp } from "../../src/09-useContext/MainApp";

describe("Pruebas en <MainApp />", () => {
  test("Debe mostrarse HomePage", () => {
    render(
      <MemoryRouter>
        <MainApp />
      </MemoryRouter>
    );

    expect(screen.getByText("HomePage")).toBeTruthy();
    // screen.debug()
  });

  test("Debe mostrar LoginPage", () => {
    render(
      <MemoryRouter initialEntries={["/login"]}>
        <MainApp />
      </MemoryRouter>
    );

    expect(screen.getByText("LoginPage")).toBeTruthy();
    // screen.debug()
  });
  test("Debe encontrar la clase active en la página actual", () => {
    render(
      <MemoryRouter>
        <MainApp />
      </MemoryRouter>
    );
    const linkActive = screen.getByText("Home");
    expect(linkActive.className).toContain("active");
    // screen.debug()
  });


});
