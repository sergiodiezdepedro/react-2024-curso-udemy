import { Link } from "react-router-dom";
import "animate.css";

const CharactersByHero = ({ alter_ego, characters }) => {
  //   if (alter_ego === characters) return <></>;
  //   return <p>{characters}</p>;
  return alter_ego === characters ? <></> : <p>{characters}</p>;
};

export const HeroCard = ({
  id,
  superhero,
  publisher,
  alter_ego,
  first_appearance,
  characters,
}) => {
  const heroImageUrl = `/heroes/${id}.jpg`;
  return (
    <>
      <div className="col animate__animated animate__fadeIn">
        <div className="card mb-3">
          <div className="row g-0">
            <div className="col-5">
              <img className="card-img" src={heroImageUrl} alt={superhero} />
            </div>
            <div className="col-7">
              <div className="card-body">
                <p className="card-title">{superhero}</p>
                <p className="card-text">{alter_ego}</p>
                <CharactersByHero
                  characters={characters}
                  alter_ego={alter_ego}
                />

                <p className="card-text">
                  <small className="text-muted">{first_appearance}</small>
                </p>
                <Link to={`/hero/${id}`}>Más info...</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
