import { useMemo } from "react";
import { useParams, Navigate, useNavigate } from "react-router-dom";
import { getHeroById } from "../helpers";
import 'animate.css';

export const HeroPage = () => {
  const { id } = useParams();

  const hero = useMemo(() => getHeroById(id), [id]);

  const navigate = useNavigate();

  const onNavigateBack = () => {
    navigate(-1);
  };

  if (!hero) {
    return <Navigate to="/" />;
  }

  return (
    <div className="row">
      <div className="col-4">
        <img
          className="img-thumbnail animate__animated animate__fadeInLeftBig"
          src={`/heroes/${id}.jpg`}
          alt={hero.superhero}
        />
      </div>
      <div className="col-8">
        <h1>{hero.superhero}</h1>
        <ul className="list-group list-group-flush mb-3">
          <li className="list-group-item">
            <strong>Alter ego:&nbsp;</strong>
            {hero.alter_ego}
          </li>
          <li className="list-group-item">
            <strong>Publisher:&nbsp;</strong>
            {hero.publisher}
          </li>
          <li className="list-group-item">
            <strong>First appearance:&nbsp;</strong>
            {hero.first_appearance}
          </li>
        </ul>
        <p className="font-weight-bold">Character/s:</p>
        <p>{hero.characters}</p>
        <button onClick={onNavigateBack} className="btn btn-outline-primary">
          Back
        </button>
      </div>
    </div>
  );
};
