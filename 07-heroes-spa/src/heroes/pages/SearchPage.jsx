import { useLocation, useNavigate } from "react-router-dom";
import queryString from "query-string";
import { useForm } from "../../hooks/useForm";
import { getHeroesByName } from "../helpers";
import "animate.css";
import { HeroCard } from "../components";

export const SearchPage = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const { q = "" } = queryString.parse(location.search);

  const heroes = getHeroesByName(q);

  const showSearchBox = q.length === 0;
  const showErrorBox = q.length > 0 && heroes.length === 0;

  const { searchText, onInputChange } = useForm({
    searchText: q,
  });

  const onSearchSubmit = (event) => {
    event.preventDefault();
    // if (searchText.trim().length < 2) return;
    navigate(`?q=${searchText}`);
  };
  return (
    <>
      <div className="animate__animated animate__fadeIn">
        <h1>Search Page</h1>
        <hr />
        <div className="row">
          <div className="col-5">
            <p className="font-weight-bold">Search</p>
            <hr />
            <form onSubmit={onSearchSubmit}>
              <input
                type="text"
                className="form-control mb-3"
                placeholder="Search hero..."
                name="searchText"
                autoComplete="off"
                value={searchText}
                onChange={onInputChange}
              />
              <button className="btn btn-primary">Search</button>
            </form>
          </div>
          <div className="col-7">
            <p className="font-weight-bold">Results</p>
            <hr />
            <div
              className="alert alert-primary animate__animated animate__fadeIn"
              style={{ display: showSearchBox ? "" : "none" }}
              aria-label="alert-danger"
            >
              Search a Hero
            </div>

            <div
              className="alert alert-danger animate__animated animate__fadeIn"
              style={{ display: showErrorBox ? "" : "none" }}
            >
              No hero with <strong>{q}</strong>
            </div>
            {heroes.map((hero) => (
              <HeroCard key={hero.id} {...hero} />
            ))}
          </div>
        </div>
      </div>
    </>
  );
};
