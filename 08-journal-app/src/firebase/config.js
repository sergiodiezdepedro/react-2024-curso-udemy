// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore/lite";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBEQUT8GIGrMRU4HRPVy2OuhjRoNNyvMak",
  authDomain: "react-curso-2024-7875e.firebaseapp.com",
  projectId: "react-curso-2024-7875e",
  storageBucket: "react-curso-2024-7875e.appspot.com",
  messagingSenderId: "366859245676",
  appId: "1:366859245676:web:d1d00ab5b7bc629f09a049",
};

// Initialize Firebase
export const FirebaseApp = initializeApp(firebaseConfig);
export const FirebaseAuth = getAuth(FirebaseApp);
export const FirebaseDB = getFirestore(FirebaseApp);
