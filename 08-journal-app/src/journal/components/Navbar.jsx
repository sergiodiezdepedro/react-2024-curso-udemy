import { useDispatch } from "react-redux";
import Toolbar from "@mui/material/Toolbar";
import AppBar from "@mui/material/AppBar/";
import IconButton from "@mui/material/IconButton";
import MenuOutlined from "@mui/icons-material/MenuOutlined";
import LogoutOutlined from "@mui/icons-material/LogoutOutlined";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { startLogout } from "../../store";

export const Navbar = ({ toggleSidebar }) => {
  const dispatch = useDispatch();

  const onLogout = () => {
    dispatch(startLogout());
  };
  return (
    <AppBar position="fixed">
      <Toolbar>
        <IconButton onClick={toggleSidebar} color="inherit" edge="start">
          <MenuOutlined />
        </IconButton>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center">
          <Typography variant="h6" noWrap component="div">
            Journal APP
          </Typography>
          <IconButton onClick={onLogout} sx={{ color: "#fff" }}>
            <LogoutOutlined />
            <Typography sx={{ marginLeft: 1, fontSize: 14 }}>
              Cerrar Sesión
            </Typography>
          </IconButton>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};
