import { useMemo } from "react";
import { useDispatch } from "react-redux";
import Grid from "@mui/material/Grid";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import TurnedInNot from "@mui/icons-material/TurnedInNot";
import { setActiveNote } from "../../store";

export const SideBarItem = ({ title = "", body, id, date, imageUrls = [] }) => {
  const dispatch = useDispatch();

  const onClickNote = () => {
    dispatch(setActiveNote({ title, body, id, date, imageUrls }));
  };

  const newTitle = useMemo(() => {
    return title.length > 8 ? title.substring(0, 8) + "..." : title;
  }, [title]);

  const newBody = useMemo(() => {
    return body.length > 15 ? body.substring(0, 15) + "..." : body;
  }, [body]);
  return (
    <ListItem disablePadding>
      <ListItemButton onClick={onClickNote}>
        <ListItemIcon>
          <TurnedInNot />
        </ListItemIcon>
        <Grid container sx={{ display: "flex", alignItems: "center" }}>
          <ListItemText primary={newTitle} />
          <ListItemText secondary={newBody} />
        </Grid>
      </ListItemButton>
    </ListItem>
  );
};
