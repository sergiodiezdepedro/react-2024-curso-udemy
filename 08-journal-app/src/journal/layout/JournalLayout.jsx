import { useState } from "react";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import { Navbar, Sidebar } from "../components";

export const JournalLayout = ({ children }) => {
  const [isActive, setIsActive] = useState(false);

  const handleClickSidebar = () => {
    setIsActive(!isActive);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <Navbar toggleSidebar={handleClickSidebar} />
      <Sidebar toggleSidebar={handleClickSidebar} isActive={isActive} />

      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <Toolbar />

        {children}
      </Box>
    </Box>
  );
};
