import { useDispatch, useSelector } from "react-redux";
import IconButton from "@mui/material/IconButton";
import AddOutlined from "@mui/icons-material/AddOutlined";
import { JournalLayout } from "../layout";
import { NoteView, NothingSelectedView } from "../views";
import { startNewNote } from "../../store";

export const JournalPage = () => {
  const dispatch = useDispatch();
  const { isSaving, active } = useSelector((state) => state.journal);
  const onclickNewNote = () => {
    dispatch(startNewNote());
  };
  return (
    <>
      <JournalLayout>
        {!!active ? <NoteView /> : <NothingSelectedView />}

        <IconButton
          onClick={onclickNewNote}
          disabled={isSaving}
          size="large"
          sx={{
            color: "#fff",
            backgroundColor: "error.main",
            ":hover": { backgroundColor: "error.main", opacity: 0.9 },
            position: "fixed",
            right: 50,
            bottom: 50,
          }}>
          <AddOutlined sx={{ fontSize: 30 }} />
        </IconButton>
      </JournalLayout>
    </>
  );
};
