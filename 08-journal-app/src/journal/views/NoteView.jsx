import { useMemo, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "../../hooks";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import SaveOutlined from "@mui/icons-material/SaveOutlined";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import UploadOutlined from "@mui/icons-material/UploadOutlined";
import DeleteOutline from "@mui/icons-material/DeleteOutline";
import { ImageGallery } from "../components";
import {
  setActiveNote,
  startSaveNote,
  startUploadingFiles,
  startDeletingNote,
} from "../../store";

export const NoteView = () => {
  const dispatch = useDispatch();
  const {
    active: note,
    messageSaved,
    isSaving,
  } = useSelector((state) => state.journal);

  const { body, title, date, onInputChange, formState } = useForm(note);

  const dateString = useMemo(() => {
    const newDate = new Date(date);
    const opciones = { year: "numeric", month: "long", day: "numeric" };
    const fechaLocal = new Intl.DateTimeFormat("es-ES", opciones).format(
      newDate
    );
    return fechaLocal;
  }, [date]);

  const fileInputRef = useRef();

  useEffect(() => {
    dispatch(setActiveNote(formState));
  }, [formState]);

  useEffect(() => {
    if (messageSaved.length > 0) {
      alert(`Nota ${messageSaved}`);
    }
  }, [messageSaved]);

  const onSaveNote = () => {
    dispatch(startSaveNote());
  };

  const onFileInputChange = ({ target }) => {
    if (target.files === 0) return;
    dispatch(startUploadingFiles(target.files));
    // console.log("subiendo archivos");
  };

  const onDelete = () => {
    dispatch(startDeletingNote());
  };

  return (
    <Grid container sx={{ marginBottom: 5 }}>
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        sx={{ marginBottom: 2 }}>
        <Grid item>
          <Typography fontSize={36} fontWeight="light">
            {dateString}
          </Typography>
        </Grid>
        <Grid item>
          <input
            type="file"
            ref={fileInputRef}
            multiple
            onChange={onFileInputChange}
            style={{ display: "none" }}
          />
          <IconButton
            color="primary"
            disabled={isSaving}
            onClick={() => fileInputRef.current.click()}>
            <UploadOutlined />
          </IconButton>
          <Button
            onClick={onSaveNote}
            color="primary"
            sx={{ padding: 2 }}
            disabled={isSaving}>
            <SaveOutlined sx={{ fontSize: 30, marginRight: 1 }} />
            Guardar
          </Button>
        </Grid>
      </Grid>
      <Grid container>
        <TextField
          type="text"
          variant="filled"
          fullWidth
          placeholder="Añade un título"
          label="Título"
          sx={{ marginBottom: 2 }}
          name="title"
          value={title}
          onChange={onInputChange}
        />
        <TextField
          type="text"
          variant="filled"
          fullWidth
          multiline
          placeholder="¿Qué sucedió hoy?"
          label="Contenido de la entrada"
          minRows={5}
          sx={{ marginBottom: 4 }}
          name="body"
          value={body}
          onChange={onInputChange}
        />
      </Grid>
      <Grid container justifyContent="end">
        <Button onClick={onDelete} sx={{ mt: 1 }} color="error">
          <DeleteOutline />
          Borrar
        </Button>
      </Grid>
      <ImageGallery images={note.imageUrls} />
    </Grid>
  );
};
