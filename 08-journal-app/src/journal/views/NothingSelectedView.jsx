import Grid from "@mui/material/Grid";
import StarOutline from "@mui/icons-material/StarOutline";
import Typography from "@mui/material/Typography";

export const NothingSelectedView = () => {
  return (
    <Grid
      container
      spacing={0}
      direction='column'
      alignItems='center'
      justifyContent='center'
      sx={{
        minHeight: "calc(100vh - 110px)",
        backgroundColor: "primary.main",
        padding: 4,
        borderRadius: 4,
      }}
    >
      <Grid item xs={12}>
        <StarOutline sx={{color: "#fff", fontSize: "100px"}}/>
      </Grid>
      <Grid item xs={12}>
        <Typography color="#fff" variant="h5">Selecciona o crea una entrada</Typography>
      </Grid>
    </Grid>
  );
};
