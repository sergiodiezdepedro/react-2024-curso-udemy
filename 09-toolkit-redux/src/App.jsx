import { useSelector, useDispatch } from "react-redux";
import { increment, decrement, incrementByAmount } from "./store/slices";
import reactLogo from "./assets/react.svg";
import "./App.css";

function App() {
  const { counter } = useSelector((state) => state.counter);
  const dispatch = useDispatch();
  return (
    <>
      <div className='App'>
        <header className='App-header'>
          <img src={reactLogo} className='App-logo' alt='logo' />
          <h2>count is: {counter}</h2>
          <p>
            <button type='button' onClick={() => dispatch(increment())}>
              Sumar 1
            </button>
          </p>
          <p>
            <button type='button' onClick={() => dispatch(decrement())}>
              Restar 1
            </button>
          </p>
          <p>
            <button
              type='button'
              onClick={() => dispatch(incrementByAmount(2))}
            >
              Sumar 2
            </button>
          </p>
        </header>
      </div>
    </>
  );
}

export default App;
