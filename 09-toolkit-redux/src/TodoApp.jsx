import { useState } from "react";
// import { useGetTodosQuery, useGetTodoQuery } from "./store/apis/todosApi";
import { useGetTodoQuery } from "./store";

export const TodoApp = () => {
  const [todoId, setTodoId] = useState(1);
  // const { data: todos = [], isLoading } = useGetTodosQuery();
  const { data: todo, isLoading } = useGetTodoQuery(todoId);

  const nextTodo = () => {
    setTodoId(todoId + 1);
  };

  const prevTodo = () => {
    if (todoId === 1) return;
    setTodoId(todoId - 1);
  };

  return (
    <>
      <h1>Todos - RTK Query</h1>
      <hr />
      <h4>Loading: {isLoading ? "True" : "False"} </h4>

      <p>{JSON.stringify(todo)}</p>

      <div className="btns-container">
        <button onClick={prevTodo}>Prev Todo</button>
        <button onClick={nextTodo}>Next Todo</button>
      </div>
      {/* 
            <ul>
                { todos.map( todo => (
                    <li key={ todo.id }>
                        <strong> { todo.completed ? 'DONE' : 'Pending' } </strong> 
                        { todo.title }
                    </li>
                ) ) }
            </ul> */}
    </>
  );
};
