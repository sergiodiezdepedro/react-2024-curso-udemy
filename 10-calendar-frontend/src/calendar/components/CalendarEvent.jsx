export const CalendarEvent = ({ event }) => {
  const { title, user } = event;
  return (
    <>
      <span>
        <small>{title}</small>
      </span>
      <span>
        &nbsp;-&nbsp;<small>{user.name}</small>
      </span>
    </>
  );
};
