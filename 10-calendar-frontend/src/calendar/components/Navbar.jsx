import { useAuthStore } from "../../hooks";

export const Navbar = () => {
  const { startLogout, user } = useAuthStore();

  return (
    <div className="navbar navbar-dark bg-dark mb-4 px-4">
      <span className="navbar-brand">
        <em className="fas fa-calendar-alt"></em>
        &nbsp; {user.name}
      </span>
      <button className="btn btn-outline-light" onClick={startLogout}>
        <em className="fas fa-sign-out-alt"></em>
        &nbsp;
        <span>Salir</span>
      </button>
    </div>
  );
};
