# React: de cero a experto (Hooks y MERN) - Udemy

## Apps y software y recursos web para el Curso

- [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=es&authuser=1)

- [Redux Devtools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=es)

- [Postman](https://www.postman.com/downloads/). También se puede usar como web app.

- [Mongo Compass](https://www.mongodb.com/try/download/compass)

- [Node](https://nodejs.org/es/)

- [Firebase](https://firebase.google.com/)

- [Cloudinary](https://cloudinary.com/). Para borrar archivos: [link](https://cloudinary.com/documentation/admin_api#delete_resources)

## Plugins de Visual Studio Code recomendados para React

- [ES7 React/Redux](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)

- [Simple React Snippets](https://marketplace.visualstudio.com/items?itemName=burkeholland.simple-react-snippets)

- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)

## Documentación general

[Create React App Scripts](https://create-react-app.dev/docs/available-scripts/)

[PWA](https://web.dev/learn/pwa/)

[robots.txt](https://developers.google.com/search/docs/crawling-indexing/robots/intro)

## Tutorial oficial de React (Tres en raya)

[Link](https://es.react.dev/learn/tutorial-tic-tac-toe)

## Create React App

https://create-react-app.dev/

**El modo más rápido**:

````bash
npx create-react-app my-app
````
## Creando app con [Vite](https://vitejs.dev/)

````bash
npm create vite nombre-app
````
## React UI tools

https://mui.com/

## Introducción

![Introducción a React](img/intro-01.png)

La imagen anterior muestra la típica mezcla de React, **JSX**

JSX = JS + XML

![JSX](img/intro-02.png)

## Babel

React utiliza profusamente [Babel](https://babeljs.io/). React emplea nuevas especificaciones JavaScript, por eso necesita Babel, que compila este nuevo JS y lo transforma en un JS más estándar, que cualquier navegador puede ejecutar.

## JavaScript moderno

### Array.prototype.map()

[Explicación en MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

### Destructuring assignment

[Explicación en MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

### Array.prototype.find()

[Explicación en MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find)

### Array.prototype.filter()

[Explicación en MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)

### Promesas
[Explicación en MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)

### Fetch API
[Explicación en MDN](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)

### Repositorio en GitHub de la Sección 3 del Curso, Introducción a JS moderno
https://github.com/Klerith/react-intro-javascript

## Objeto de evento en React
[Link](https://es.react.dev/reference/react-dom/components/common#react-event-object)

## Hooks de React
[Link](https://es.react.dev/reference/react/hooks)

## Tests

Las pruebas unitarias están enfocadas en pequeñas funcionalidades. 

Las pruebad de integración se centran en el cómo reaccionan varias piezas en conjunto.

**AAA**:

- Arrange (Arreglar). Se prepara el estado inicial.
- Act (Actuar). Se aplican acciones o estímulos.
- Assert (Afirmar). Observar el comportamiento resultante.

### Jest
[Documentación oficial](https://jestjs.io/)

### React Testing Library
[Link](https://testing-library.com/)

## Estructura de archivos de un proyecto con React

- [Link Página Oficial React](https://es.legacy.reactjs.org/docs/faq-structure.html)

- [HackerNoon](https://hackernoon.com/structuring-projects-and-naming-components-in-react-1261b6e18d76)

## React `<StrictMode>`
[Link](https://react.dev/reference/react/StrictMode)

## React Hooks
[Documentación oficial](https://es.react.dev/reference/react/hooks)

## useReducer
- [Documentación oficial](https://es.react.dev/reference/react/useReducer)
- [How to Use the useReducer Hook in React](https://www.freecodecamp.org/news/react-usereducer-hook/)

## useContext
- [Documentación oficial](https://es.react.dev/reference/react/useContext)
- [Pasar datos en profundidad con contexto](https://es.react.dev/learn/passing-data-deeply-with-context)

## React Router
https://reactrouter.com/

[Migrar React Router de V5 a V6](https://www.youtube.com/watch?v=FR7x0tqwafc)

## Redux

[Redux](https://redux.js.org/) es un contenedor predecible del estado de nuesta aplicación.

![Fundamentos de Redux](img/redux-01.png)

Así funciona el Reducer:

![Reducer](img/reducer.png)

Redux lo hace así si el proceso es síncrono:

![Funcionamiento de Redux síncrono](img/redux-02.png)

Si el proceso es asíncrono, Redux lo maneja de la siguiente manera:

![Funcionamiento de Redux asíncrono](img/redux-03.png)

Desarrollando el patrón Redux con Redux Toolkit: [documentación oficial](https://redux-toolkit.js.org/).

### RTK Query

Es una herramienta para la obtención y almacenamiento en caché de datos. Está diseñada para simplificar los casos comunes de carga de datos en una aplicación web, eliminando la necesidad de escribir a mano la lógica de obtención y almacenamiento en caché de datos.

Es parte de Redux Toolkit.

[Documentación oficial](https://redux-toolkit.js.org/rtk-query/overview)

## Variables de entorno en React

[Documentación oficial](https://create-react-app.dev/docs/adding-custom-environment-variables/)

## Backend con MongoDB, ExpressJs y NodeJS

[HTTP Status Codes](https://www.restapitutorial.com/httpstatuscodes)

Para validaciones en Express existe el paquete **express-validator**.

[MongoDB Atlas](https://www.mongodb.com/)

[Mongoose](https://mongoosejs.com/)

[bcryptjs](https://www.npmjs.com/package/bcryptjs): encripta la contraseña del usuario.

[JSON Web Token](https://jwt.io/): generar token de validación, que pueden expirar en un tiempo concreto.

[cors](https://www.npmjs.com/package/cors): CORS is a node.js package for providing a [Connect/Express](http://expressjs.com/) middleware that can be used to enable [CORS](http://en.wikipedia.org/wiki/Cross-origin_resource_sharing) with various options.

[Moment.js](https://www.npmjs.com/package/moment): es una librería JS de fechas para parsear, validar, manipular y formatear fechas.

## Todo el código final

Enlace al repositorio en el que se encuentra el código final de la app Calendar, con todo el front y el back:

https://github.com/DevTalles-corp/React-MERN-backend

## Top 10 React component libraries for 2020

[Link](https://blog.logrocket.com/top-10-react-component-libraries-for-2020/)

## Youtube playlist de React + TypeScript

[Link](https://www.youtube.com/watch?v=dNxaP_BTtwQ&list=PLCKuOXG0bPi26-eawizqyLOgM7j66H_4M)
