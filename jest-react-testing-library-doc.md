# Instalación y configuracion de Jest + React Testing Library
## En proyectos de React + Vite

1. Instalaciones:
```bash
yarn add --dev jest babel-jest @babel/preset-env @babel/preset-react 
yarn add --dev @testing-library/react @types/jest jest-environment-jsdom
```

2. Opcional: Si usamos `Fetch API` en el proyecto:
```bash
yarn add --dev whatwg-fetch
```

3. Actualizar los scripts del `package.json`
```json
"scripts: {
  ...
  "test": "jest --watchAll"
```

4. Crear la configuración de babel `babel.config.js`
```javascript
module.exports = {
    presets: [
        [ '@babel/preset-env', { targets: { esmodules: true } } ],
        [ '@babel/preset-react', { runtime: 'automatic' } ],
    ],
};
```

5. Opcional, pero eventualmente necesario, crear `jest.config` y `jest.setup`:

**jest.config.js**
```javascript
module.exports = {
    testEnvironment: 'jest-environment-jsdom',
    setupFiles: ['./jest.setup.js']
}
```

**jest.setup.js**
```javascript
// En caso de necesitar la implementación del FetchAPI
import 'whatwg-fetch'; // <-- yarn add whatwg-fetch
```

Para pruebas finales de todo, con el front y back finales, revisar este gist:

https://gist.github.com/Klerith/b2eafa2a5fb9f09d6d043781be976e06

Enlace al repositorio donde queda el código final tras la última sección de Testing:

https://github.com/Klerith/react-mern-calendar/tree/fin-seccion-29